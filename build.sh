#! /bin/sh

set -u

g++ -I ./include -std=c++2b -pedantic -pedantic-errors -Wall -Wextra -Wold-style-cast -g -march=native -flto -fno-math-errno -ffp-contract=fast -O3 -fno-exceptions color.cc -o color
