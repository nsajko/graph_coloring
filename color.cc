// Copyright © 2022 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <numeric>
#include <span>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <graph.h>

namespace {

auto write_dot_to_file(const std::string name, const std::vector<graph::ColoredGraph> &g) -> void {
	std::ofstream s{name};
	graph::print_dot(s, g);
}

template<std::invocable<> NextGraphFun>
auto color_and_print_sorted_for_each_graph(NextGraphFun next, const std::string &ident, std::ostream &ost) -> void {
	using namespace graph;

	using ColCompsWithMatr = std::pair<SquareMatrix, std::vector<ColoredGraph>>;
	std::vector<ColCompsWithMatr> v{};
	v.reserve(100);

	// Color all the graphs.
	for (;;) {
		Graph g{next()};

		// A graph with a single node means we are done.
		if (g.adj_list.size() == 1) {
			break;
		}

		v.push_back({g.adj_matr, colored_components(g.connected_components())});
	}

	// Sort by chromatic number and number of edges.
	std::ranges::stable_sort(v, {}, [](const ColCompsWithMatr &g) -> long {
		const std::vector<ColoredGraph> c{g.second};
		return (static_cast<long>(max_colors(c)) << 15) + edge_count_sum(c);
	});

	// Check coloring.
	int graph_index{0};
	for (const ColCompsWithMatr &g: v) {
		constexpr auto inval{[](const ColoredGraph &cg) -> bool {
			return !cg.is_valid();
		}};
		if (std::ranges::any_of(g.second, inval)) {
			std::cerr << "faulty coloring: " << graph_index << '\n';
		}
	}

	// Output.
	graph_index = 0;
	for (const ColCompsWithMatr &g: v) {
		// Chromatic number
		int ch_num{max_colors(g.second)};

		// Filename
		std::stringstream name_s{};
		name_s << ident << '_' << std::setfill('0') << std::setw(5) <<
		          graph_index << '_' << std::setfill('0') << std::setw(2) << ch_num;
		std::string name{name_s.str()};

		// Print DOT.
		std::stringstream dot_name_s{};
		dot_name_s << "dot/" << name << ".dot";
		write_dot_to_file(dot_name_s.str(), g.second);

		// Print markdown.
		ost << "## Graph " << graph_index << "\n\n```\n" << g.first << "```\n\n"
		       "Its chromatic number is " << ch_num << ".\n\n"
		       "The colored graph as drawn by circo and dot:\n\n"
		       "![drawn by circo](png/" << name << "_circo.png \"drawn by circo\")\n\n"
		       "![drawn by dot](png/" << name << "_dot.png \"drawn by dot\")\n\n";

		graph_index++;
	}
}

auto generate_and_color_and_print_sorted(int num_graphs, int num_nodes, double pr) {
	using namespace graph;

	std::stringstream id{};
	id << "random_" << std::setfill('0') << std::setw(2) << num_nodes << ',' << pr;

	std::stringstream markdown_file_name{};
	markdown_file_name << "examples_" << id.str() << ".md";

	std::ofstream ost{markdown_file_name.str()};

	ost << "# Examples (" << num_nodes << ',' << pr << ")\n\n" << num_graphs <<
	       " graphs, with " << num_nodes << " nodes each. The probability of any edge "
	       "existing is " << pr << ".\n\n\n";

	std::cout << "[Examples (" << num_nodes << ',' << pr << ")](" << markdown_file_name.str() <<
	             " \"some examples\")\n\n";

	RandomGraph r{num_nodes, pr};

	auto lambda{[&r, &num_graphs] -> Graph {
		if (num_graphs == 0) {
			return Graph{1, std::vector<int8_t>(1, 0)};
		}
		num_graphs--;
		return r.next();
	}};

	color_and_print_sorted_for_each_graph(lambda, id.str(), ost);
}

}  // namespace

auto main(int argc, char **argv) -> int {
	std::ios::sync_with_stdio(false);

	if (argc != 1 + 3) {
		std::cerr << "error: there must be 3 arguments\n";
		return 1;
	}

	// Number of graphs
	int n{std::atoi(argv[1])};
	if (n < 1) {
		std::cerr << "error: the number of graphs must be positive\n";
		return 1;
	}

	// Number of nodes in each graph
	int num_nodes{std::atoi(argv[2])};
	if (num_nodes < 1) {
		std::cerr << "error: the number of nodes must be positive\n";
		return 1;
	}

	// Edge probability
	double pr{std::atoi(argv[3]) / static_cast<double>(64)};
	if (pr < 0 || 1 <= pr) {
		std::cerr << "error: a probability must lie between 0 and 1\n";
		return 1;
	}

	generate_and_color_and_print_sorted(n, num_nodes, pr);
}
