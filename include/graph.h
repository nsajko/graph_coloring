#pragma once

// Copyright © 2022 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <numeric>
#include <span>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/multiprecision/cpp_int.hpp>

#include <pseudorandom_generation.h>

namespace graph {

class SquareMatrix {
	int n;

	std::vector<int8_t> m;

public:

	SquareMatrix(int n): n{n}, m(n*n, 0) {}

	SquareMatrix(int n, const std::vector<int8_t> &m): n{n}, m{m} {}

	SquareMatrix(const std::vector<std::vector<int8_t>> &rows): n(rows.size()), m(n*n, 0) {
		for (int i{0}; i < n; i++) {
			for (int j{i + 1}; j < n; j++) {
				(*this)(i, j) = (*this)(j, i) = rows[i][j];
			}
		}
	}

	// Interpreted as undirected graph adjacency list, with edge uv
	// contracted.
	//
	// u must be less than v.
	SquareMatrix(const SquareMatrix &sm, int u, int v): n(sm.n_cols()) {
		// Merge node v into node u.

		// for (int i{0}; i < u; i++) {
		// 	for (int j{i + 1}; j < v; j++) {
		// 		new_matr(i, j) = new_matr(j, i) = adj_matr(i, j);
		// 	}
		// 	new_matr(u, i) |= adj_matr(i, v);
		// 	new_matr(i, u) |= adj_matr(i, v);
		// 	for (int j{v}; j < new_matr.n_cols(); j++) {
		// 		new_matr(i, j) = new_matr(j, i) = adj_matr(i, j + 1);
		// 	}
		// }

		// for (int i{u + 1}; i < v; i++) {
		// 	new_matr(u, i) = new_matr(i, u) = adj_matr(i, u) | adj_matr(i, v);
		// 	for (int j{i + 1}; j < v; j++) {
		// 		new_matr(i, j) = new_matr(j, i) = adj_matr(i, j);
		// 	}
		// 	for (int j{v}; j < new_matr.n_cols(); j++) {
		// 		new_matr(i, j) = new_matr(j, i) = adj_matr(i, j + 1);
		// 	}
		// }

		// for (int i{v}; i < new_matr.n_cols(); i++) {
		// 	new_matr(u, i) = new_matr(i, u) = adj_matr(i + 1, u) | adj_matr(i + 1, v);
		// 	for (int j{i + 1}; j < new_matr.n_cols(); j++) {
		// 		new_matr(i, j) = new_matr(j, i) = adj_matr(i, j + 1);
		// 	}
		// }

		// Represent sm as a vector of rows.
		std::vector<std::vector<int8_t>> rows(sm.n_cols());
		for (std::vector<int8_t> &row: rows) {
			row = std::vector<int8_t>(n, 0);
		}
		for (int i{0}; i < n; i++) {
			for (int j{i + 1}; j < n; j++) {
				rows[i][j] = rows[j][i] = sm(i, j);
			}
		}

		// Add edges of node v to node u.
		for (int i{0}; i < n; i++) {
			rows[i][u] |= rows[v][i];
			rows[u][i] |= rows[v][i];
		}

		// Remove loop edge.
		rows[u][u] = 0;

		// Remove node v.
		rows.erase(rows.begin() + v);
		n--;
		for (std::vector<int8_t> &row: rows) {
			row.erase(row.begin() + v);
		}

		*this = SquareMatrix(rows);
	}

	// Number of columns
	auto n_cols() const -> int {
		return n;
	}

	auto operator()(int i, int j) const -> int8_t {
		return m[i*n + j];
	}

	auto operator()(int i, int j) -> int8_t & {
		return m[i*n + j];
	}

	auto swap(SquareMatrix &rhs) -> void {
		std::swap(n, rhs.n);
		std::swap(m, rhs.m);
	}
};

auto swap(SquareMatrix &lhs, SquareMatrix &rhs) -> void {
	lhs.swap(rhs);
}

auto operator<<(std::ostream &os, const SquareMatrix &sm) -> std::ostream & {
	for (int i{0}; i < sm.n_cols(); i++) {
		os << static_cast<int>(sm(i, 0));
		for (int j{1}; j < sm.n_cols(); j++) {
			os << ',' << static_cast<int>(sm(i, j));
		}
		os << '\n';
	}

	return os;
}

class AdjacencyList {
	std::vector<std::vector<int8_t>> l{};

	long number_of_edges{0};

public:

	AdjacencyList() {}

	AdjacencyList(const SquareMatrix &adj_mat): l(adj_mat.n_cols()) {
		for (int i{0}; i < l.size(); i++) {
			for (int j{0}; j < l.size(); j++) {
				if (adj_mat(i, j) != 0) {
					l[i].push_back(j);
				}
			}
		}

		number_of_edges = std::transform_reduce(l.cbegin(), l.cend(), static_cast<long>(0),
		                                        std::plus<>(),
		                                        [](const std::vector<int8_t> &v) -> long {
			return v.size();
		});
	}

	auto size() const -> int {
		return l.size();
	}

	auto num_edges() const -> long {
		return number_of_edges;
	}

	auto neighborhood(int n) const -> const std::vector<int8_t> & {
		return l[n];
	}

	auto connect(int u, int v) -> void {
		namespace r = std::ranges;
		l[u].push_back(v);
		l[v].push_back(u);
		r::sort(l[u]);
		r::sort(l[v]);
		number_of_edges++;
	}
};

auto vector_from_bitset(uintmax_t s) -> std::vector<int8_t> {
	std::vector<int8_t> ret{};
	for (int i{0}; s != 0;) {
		if ((s & 1U) != 0) {
			ret.push_back(i);
		}
		i++;
		s >>= 1;
	}
	return ret;
}

// Toeplitz matrix, for constructing Toeplitz graphs.
struct Toeplitz {
	int n;
	std::vector<int8_t> d;

	Toeplitz(int n, const std::vector<int8_t> &d): n{n}, d{d} {}

	Toeplitz(int n, uintmax_t bitset): n{n}, d{vector_from_bitset(bitset)} {}
};

auto seq(int n) -> std::vector<int8_t> {
	std::vector<int8_t> ret(n);
	for (int i{0}; i < n; i++) {
		ret[i] = i;
	}
	return ret;
}

struct Graph {
	// Adjacency matrix
	SquareMatrix adj_matr;

	std::vector<int8_t> node_labels;

	// Adjacency list
	AdjacencyList adj_list;

	// Square adjacency matrix constructor.
	Graph(int n, const std::vector<int8_t> &m): adj_matr{n, m}, node_labels{seq(n)}, adj_list{adj_matr} {}

	// Toeplitz adjacency matrix constructor.
	Graph(const Toeplitz &t): adj_matr{t.n}, node_labels{seq(t.n)} {
		for (int8_t e: t.d) {
			// Set all members of the adjacency matrix on
			// the diagonal specified by e.
			for (int i{0}; i + e < t.n; i++) {
				adj_matr(i, i + e) = adj_matr(i + e, i) = 1;
			}
		}
		adj_list = AdjacencyList{adj_matr};
	}

	// Subgraph induced in g by nodes.
	Graph(const Graph &g, std::vector<int8_t> nodes): adj_matr(nodes.size()), node_labels{nodes} {
		// Inverse mapping. Translates old node labels to new
		// ones.
		std::vector<int8_t> nodes_inverse(g.adj_list.size(), -128);
		for (int i{0}; i < nodes.size(); i++) {
			nodes_inverse[nodes[i]] = i;
		}

		// Nonzero means the node is to be included in the
		// subgraph.
		std::vector<int8_t> node_is_included(g.adj_list.size(), 0);
		for (int8_t node: nodes) {
			node_is_included[node] = 1;
		}

		for (int8_t node: nodes) {
			for (int8_t neigh: g.adj_list.neighborhood(node)) {
				if (node_is_included[neigh] != 0) {
					int i{nodes_inverse[node]},
					    j{nodes_inverse[neigh]};
					adj_matr(i, j) = 1;
				}
			}
		}

		adj_list = AdjacencyList{adj_matr};
	}

	auto connected_components() const -> std::vector<Graph> {
		std::vector<Graph> ret{};

		// Nonzero means the node was visited by the DFS.
		std::vector<int8_t> visited(adj_list.size(), 0);

		// The nodes of a connected component.
		std::vector<int8_t> nodes{};

		// DFS stack.
		std::vector<int8_t> walk{};

		for (int start{0}; start < adj_list.size(); start++) {
			// DFS: https://idea-instructions.com/graph-scan.svg
			if (visited[start] != 0) {
				continue;
			}
			visited[start] = 1;
			nodes.push_back(start);
			walk.push_back(start);
			for (; !walk.empty();) {
				int node{walk.back()};
				int done_with_node{true};
				for (int neigh: adj_list.neighborhood(node)) {
					if (visited[neigh] == 0) {
						done_with_node = false;
						visited[neigh] = 1;
						nodes.push_back(neigh);
						walk.push_back(neigh);
						break;
					}
				}
				if (done_with_node) {
					walk.pop_back();
				}
			}

			std::ranges::sort(nodes);
			ret.emplace_back(*this, nodes);
			nodes.clear();
		}

		return ret;
	}

	// Connects the nodes u and v.
	auto connect(int u, int v) -> void {
		adj_matr(u, v) = 1;
		adj_matr(v, u) = 1;

		adj_list.connect(u, v);
	}

	auto contract(int u, int v) -> void {
		if (v < u) {
			std::swap(u, v);
		}

		node_labels.erase(node_labels.begin() + v);

		// Merge node v into node u.
		adj_matr = SquareMatrix{adj_matr, u, v};

		adj_list = AdjacencyList{adj_matr};
	}

	// Prints a DOT representation of the network nodes.
	auto print_dot_nodes(std::ostream &os) const -> void {
		for (int i{0}; i < adj_list.size(); i++) {
			int label{node_labels[i]};
			os << ' ' << label << '\n';
		}
	}

	// Prints a DOT representation of the network edges.
	auto print_dot_edges(std::ostream &os) const -> void {
		for (int i{0}; i < adj_list.size(); i++) {
			for (int j{i + 1}; j < adj_list.size(); j++) {
				if (adj_matr(i, j) != 0) {
					int a{node_labels[i]},
					    b{node_labels[j]};
					os << ' ' << a << " -- " << b << '\n';
				}
			}
		}
	}
};

// Prints the graph in GraphViz DOT format.
auto print_dot(std::ostream &os, const Graph &g) -> void {
	os << "graph {\n";
	g.print_dot_nodes(os);
	os << '\n';
	g.print_dot_edges(os);
	os << "}\n";
}

class ColoredGraph {
public:

	Graph g;

private:

	int num_colors;
	std::vector<int8_t> coloring;

public:

	ColoredGraph(const Graph &g, int n, const std::vector<int8_t> &c): g{g}, num_colors{n}, coloring{c} {}

	ColoredGraph(const Graph &g, const std::vector<std::vector<int8_t>> &c): g{g}, num_colors(c.size()),
	                                                                         coloring(g.adj_list.size()) {
		for (int col{0}; col < c.size(); col++) {
			for (int8_t node: c[col]) {
				coloring[node] = col;
			}
		}
	}

	auto get_num_colors() const -> int {
		return num_colors;
	}

	// Indicates whether the coloring is valid.
	auto is_valid() const -> bool {
		for (int i{0}; i < g.adj_list.size(); i++) {
			for (int j{i + 1}; j < g.adj_list.size(); j++) {
				if ((coloring[i] == coloring[j]) && (g.adj_matr(i, j) != 0)) {
					return false;
				}
			}
		}
		return true;
	}

	auto print_dot(std::ostream &os) const -> void {
		for (int i{0}; i < g.adj_list.size(); i++) {
			double hue{coloring[i] / static_cast<double>(num_colors)};
			int label{g.node_labels[i]};
			os << ' ' << label << " [ fillcolor = \"" << hue << " 1 1\" ]\n";
		}
		os << '\n';
		g.print_dot_edges(os);
	}
};

auto max_colors(const std::vector<ColoredGraph> &graphs) -> int {
	return std::ranges::max(graphs, {}, [](const ColoredGraph &cg) -> int {
		return cg.get_num_colors();
	}).get_num_colors();
}

auto node_count_sum(const std::vector<ColoredGraph> &g) -> int {
	return std::transform_reduce(g.cbegin(), g.cend(), 0, std::plus<>(),
	                             [](const ColoredGraph &cg) -> int {
		return cg.g.adj_list.size();
	});
}

auto edge_count_sum(const std::vector<ColoredGraph> &g) -> int {
	return std::transform_reduce(g.cbegin(), g.cend(), 0, std::plus<>(),
	                             [](const ColoredGraph &cg) -> int {
		return cg.g.adj_list.num_edges();
	});
}

// Prints the colored connected components as a GraphViz DOT graph.
auto print_dot(std::ostream &os, const std::vector<ColoredGraph> &graphs) -> void {
	os << "graph {\n label=\"" << max_colors(graphs) <<
	      " colors used for coloring the " << node_count_sum(graphs) <<
	      " nodes\"\n node [ style = filled ]\n\n";
	for (const ColoredGraph &g: graphs) {
		g.print_dot(os);
		os << '\n';
	}
	os << "}\n";
}

auto graph_is_edgeless(const Graph &g) -> bool {
	for (int node{0}; node < g.adj_list.size(); node++) {
		if (!g.adj_list.neighborhood(node).empty()) {
			return false;
		}
	}
	return true;
}

auto full_set_with_this_many_members(int n) -> uintmax_t {
	// A char is 8 bits, sizeof(char) is 1.
	return (~static_cast<uintmax_t>(0) >> (8*sizeof(uintmax_t) - n));
}

template<typename T>
class IndexedByIntAndSubset {
	int m;
	int n;
	int s;
	std::vector<T> t;

public:

	// n is the number of elements in the set.
	IndexedByIntAndSubset(int m, int n): m{m}, n{n}, s(1UL << n), t(m*s, static_cast<T>(1)) {}

	auto size() const -> int {
		return m;
	}

	auto cardinality() const -> int {
		return n;
	}

	auto operator()(int i, uintmax_t set) const -> const T & {
		return t[i*s + set];
	}

	auto operator()(int i, uintmax_t set) -> T & {
		return t[i*s + set];
	}
};

template<typename T>
auto operator<<(std::ostream &os, const IndexedByIntAndSubset<T> &t) -> std::ostream & {
	uintmax_t full_set{full_set_with_this_many_members(t.cardinality())};
	for (int i{0}; i < t.size(); i++) {
		os << t(i, 0);
		for (uintmax_t j{1}; j <= full_set; j++) {
			os << ',' << t(i, j);
		}
		os << '\n';
	}

	return os;
}

auto complement_is_independent_set(const Graph &g, uintmax_t complement) -> bool {
	uintmax_t set{full_set_with_this_many_members(g.adj_list.size()) & ~complement};

	if (set == 0) {
		return true;
	}

	uintmax_t orig_set{set};
	for (int node{0}; !std::has_single_bit(set); set >>= 1, node++) {
		if ((set & 1) == 0) {
			continue;
		}
		for (int neigh: g.adj_list.neighborhood(node)) {
			if ((orig_set & (1UL << neigh)) != 0) {
				return false;
			}
		}
	}
	return true;
}

using BigInt = boost::multiprecision::cpp_int;

// From "Exact Exponential Algorithms" by Fedor V. Fomin and Dieter Kratsch.
auto subset_avoiding_subfamily_cardinalities_tables(const Graph &g) -> IndexedByIntAndSubset<BigInt> {
	int n{g.adj_list.size()};
	uintmax_t full_set{full_set_with_this_many_members(n)};
	IndexedByIntAndSubset<BigInt> ret{n + 1, n};

	// Fill table 0.
	for (uintmax_t set{0}; set <= full_set; set++) {
		ret(0, set) = complement_is_independent_set(g, set);
	}

	// Fill tables 1 to n.
	for (int i{1}; i <= n; i++) {
		for (uintmax_t set{0}; set <= full_set; set++) {
			ret(i, set) = ret(i - 1, set);
			uintmax_t e{1UL << (i - 1)};
			if ((set & e) == 0) {
				ret(i, set) += ret(i - 1, set | e);
			}
		}
	}
	return ret;
}

auto power_of_minus_one(int n) -> int {
	return 1 - ((n & 1) << 1);
}

auto cover_count_is_positive(const IndexedByIntAndSubset<BigInt> &main_table, IndexedByIntAndSubset<BigInt> &powers) -> bool {
	// Universal set cardinality.
	int card{main_table.cardinality()};

	uintmax_t full_set{full_set_with_this_many_members(card)};

	for (uintmax_t set{0}; set <= full_set; set++) {
		powers(0, set) *= main_table(card, set);
	}

	BigInt sum{0};
	for (uintmax_t set{0}; set <= full_set; set++) {
		sum += power_of_minus_one(std::popcount(set)) * powers(0, set);
	}
	return 0 < sum;
}

auto chromatic_number(const Graph &g) -> int {
	// Graph node set cardinality.
	int card{g.adj_list.size()};

	if (card == 1) {
		return 1;
	}

	const IndexedByIntAndSubset<BigInt> mem{subset_avoiding_subfamily_cardinalities_tables(g)};

	// HACK: members initialized to one in the constructor.
	IndexedByIntAndSubset<BigInt> powers{1, card};

	int q{1};
	for (; !cover_count_is_positive(mem, powers); q++) {}
	return q;	
}

// Colors the graph g with chromatic number k.
//
// The graph is not preserved.
auto colored(Graph &g, int k) -> std::vector<std::vector<int8_t>> {
	std::vector<std::vector<int8_t>> ret(g.adj_list.size());
	for (int i{0}; i < g.adj_list.size(); i++) {
		ret[i].push_back(i);
	}

	std::vector<int8_t> scratch{};

	for (;;) {
		// Choose a pair of non-adjacent nodes. If the graph is
		// complete, on the other hand, we are done.
		int u, v;
		for (u = 0; u < g.adj_list.size(); u++) {
			for (v = u + 1; v < g.adj_list.size(); v++) {
				if (g.adj_matr(u, v) == 0) {
					goto FOUND_INDEP_PAIR;
				}
			}
		}

		// The graph is complete.
		break;

FOUND_INDEP_PAIR:

		g.connect(u, v);

		if (k != chromatic_number(g)) {
			// Merge the u and v color classes.
			scratch.resize(ret[u].size() + ret[v].size());
			std::ranges::merge(ret[u], ret[v], scratch.begin());
			std::swap(scratch, ret[u]);
			ret.erase(ret.begin() + v);

			g.contract(u, v);
		}
	}

	// Diagnostics, just in case.
	if (ret.size() != k || g.adj_list.size() != k) {
		std::cout << "faulty coloring:  " << ret.size() << ' ' << k << ' ' << g.adj_list.size() << '\n';
	}

	return ret;
}

// Colors the graph g.
//
// The graph is not preserved.
//
// Algorithm description:
//
// If adding an edge between two non-adjacent nodes {u, v} increases
// the chromatic number, u and v belong to the same color class.
// Contract them.
//
// Otherwise (if the chromatic number is unchanged), keep the edge uv
// and go back to the beginning, choosing another pair of non-adjacent
// nodes.
//
// Repeat until a complete graph is obtained.
auto colored(Graph &g) -> std::vector<std::vector<int8_t>> {
	return colored(g, chromatic_number(g));
}

auto colored_components(const std::vector<Graph> &components) -> std::vector<ColoredGraph> {
	constexpr auto map{[](const Graph &gr) -> ColoredGraph {
		Graph g{gr};
		return ColoredGraph{gr, colored(g)};
	}};

	std::vector<ColoredGraph> ret{};
	std::ranges::transform(components, std::back_inserter(ret), map);
	return ret;
}

class RandomGraph {
	// Number of nodes
	int num_nodes;

	// Probability of an edge existing between a pair of nodes.
	//
	// Must be in the range [0, 1].
	double prob;

	uint64_t prob_scaled;

	// Pseudorandom generator
	pseudorandom_generation::XoshiroConstantWithStorage<
	  pseudorandom_generation::Xoshiro256PlusPlusConstantDefaultSeed,
	  1 << 14> prg{};

	static constexpr
	auto exp2(int n) -> double {
		double ret{1};
		for (int i{0}; i < n; i++) {
			ret *= 2;
		}
		return ret;
	}

	static
	auto scale_prob(double p) -> uint64_t {
		// Ensure p is in the range [0, 1].
		if (p <= 0 || 1 <= p || p != p) {
			p = 0;
		}

		constexpr double pow2{exp2(8 * sizeof(uint64_t))};

		// Scale from [0, 1) to [0, pow2).
		return p * pow2;
	}

public:

	RandomGraph(int n, double p): num_nodes{n}, prob{p}, prob_scaled{scale_prob(p)} {}

	auto next() -> Graph {
		int n{num_nodes};

		Graph g{n, std::vector<int8_t>(n*n, 0)};

		for (int i{0}; i < n; i++) {
			for (int j{i + 1}; j < n; j++) {
				if (prg.next() < prob_scaled) {
					g.connect(i, j);
				}
			}
		}

		return g;
	}
};

}  // namespace graph
